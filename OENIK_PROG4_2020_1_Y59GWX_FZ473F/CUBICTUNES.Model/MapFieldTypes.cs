﻿// <copyright file="MapFieldTypes.cs" company="Obuda University John von Neumann Faculty of Informatics">
// Copyright (c) Obuda University John von Neumann Faculty of Informatics. All rights reserved.
// </copyright>

namespace CUBICTUNES.Model
{
    /// <summary>
    /// Gets the types of the map fields.
    /// </summary>
    public enum CubeTypes
    {
        /// <summary>
        /// Player object.
        /// </summary>
        Player,

        /// <summary>
        /// Exit object, cannot be moved.
        /// </summary>
        Exit,

        /// <summary>
        /// Stationary object, cannot be moved.
        /// </summary>
        Stationary,

        /// <summary>
        /// Movable purple cubes.
        /// </summary>
        Purple,

        /// <summary>
        /// Movable green cubes.
        /// </summary>
        Green,

        /// <summary>
        /// Movable red cubes.
        /// </summary>
        Red,

        /// <summary>
        /// Movable blue cubes.
        /// </summary>
        Blue,
    }
}
