﻿// <copyright file="GameModel.cs" company="Obuda University John von Neumann Faculty of Informatics">
// Copyright (c) Obuda University John von Neumann Faculty of Informatics. All rights reserved.
// </copyright>

namespace CUBICTUNES.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Timers;
    using System.Windows;

    /// <summary>
    /// This class is the game's model. It contains the game's preferences.
    /// </summary>
    public class GameModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        /// </summary>
        /// <param name="gameWidth">Game width.</param>
        /// <param name="gameHeight">Game height.</param>
        /// <param name="blockSize">Block's size.</param>
        public GameModel(double gameWidth, double gameHeight, double blockSize)
        {
            this.GameWidth = gameWidth;
            this.GameHeight = gameHeight;
            this.BlockSize = blockSize;
            this.PushX = Math.Cos(Math.PI * 30 / 180) * blockSize;
            this.PushY = Math.Sin(Math.PI * 30 / 180) * blockSize;
        }

        /// <summary>
        /// Gets or sets the display's width.
        /// </summary>
        public double GameWidth { get; set; }

        /// <summary>
        /// Gets or sets the display's height.
        /// </summary>
        public double GameHeight { get; set; }

        /// <summary>
        /// Gets or sets one size of a block.
        /// </summary>
        public double BlockSize { get; set; }

        /// <summary>
        /// Gets or sets the ammount the object has to be pushed in the X axis.
        /// </summary>
        public double PushX { get; set; }

        /// <summary>
        /// Gets or sets the ammount the object has to be pushed in the Y axis.
        /// </summary>
        public double PushY { get; set; }

        /// <summary>
        /// Gets or sets the matrix that represents the map.
        /// </summary>
        public MapObject[,] Map { get; set; }

        /// <summary>
        /// Gets or sets a single floor.
        /// </summary>
        public MapObject[,] MapBase { get; set; }

        /// <summary>
        /// Gets or sets the player's position.
        /// </summary>
        public Point Player { get; set; }

        /// <summary>
        /// Gets or sets exit's position.
        /// </summary>
        public Point Exit { get; set; }

        /// <summary>
        /// Gets or sets the amount of stars the player got after the end of the level. (Depends on how fast it was completed.)
        /// </summary>
        public int Stars { get; set; }

        /// <summary>
        /// Gets or sets the player's time.
        /// </summary>
        public Timer PlayerTime { get; set; }

        /// <summary>
        /// Gets or sets the maximum elapsed seconds for three stars.
        /// </summary>
        public int ThreeStarSeconds { get; set; }

        /// <summary>
        /// Gets or sets the maximum elapsed seconds for two stars.
        /// </summary>
        public int TwoStarSeconds { get; set; }

        /// <summary>
        /// Gets or sets the maximum elapsed seconds for one star.
        /// </summary>
        public int OneStarSeconds { get; set; }
    }
}
