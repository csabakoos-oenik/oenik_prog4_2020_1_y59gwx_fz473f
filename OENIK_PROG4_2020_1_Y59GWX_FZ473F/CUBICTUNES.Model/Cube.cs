﻿// <copyright file="Cube.cs" company="Obuda University John von Neumann Faculty of Informatics">
// Copyright (c) Obuda University John von Neumann Faculty of Informatics. All rights reserved.
// </copyright>

namespace CUBICTUNES.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// This class represents a cubic object on the map.
    /// </summary>
    public class Cube : MapObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Cube"/> class.
        /// </summary>
        /// <param name="x">X coordinate.</param>
        /// <param name="y">Y coordinate.</param>
        /// <param name="lenght">Floor tile's side's lenght.</param>
        /// <param name="height">The number of cubes on top of each other.</param>
        /// <param name="type">The cube's type.</param>
        public Cube(double x, double y, double lenght, int height, CubeTypes type)
            : base(x, y, 2 * Math.Cos(Math.PI * 30 / 180) * lenght, 2 * Math.Sin(Math.PI * 30 / 180) * lenght)
        {
            this.SideLenght = lenght;
            this.PushX = Math.Cos(Math.PI * 30 / 180) * lenght;
            this.PushY = Math.Sin(Math.PI * 30 / 180) * lenght;
            this.Height = height;
            this.CubeType = type;
        }

        /// <summary>
        /// Gets or sets the cube's type.
        /// </summary>
        public CubeTypes CubeType { get; set; }

        /// <summary>
        /// Gets or sets the number of cubes on top of each other.
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// Gets or sets the ammount the object has to be pushed in the X axis.
        /// </summary>
        public double PushX { get; set; }

        /// <summary>
        /// Gets or sets the ammount the object has to be pushed in the Y axis.
        /// </summary>
        public double PushY { get; set; }

        /// <summary>
        /// Gets or sets the lenght of a tile's side.
        /// </summary>
        public double SideLenght { get; set; }

        /// <summary>
        /// Returns the floor tile.
        /// </summary>
        /// <returns>Floor tile geometry.</returns>
        public Geometry GetGeometry()
        {
            List<Point> points = new List<Point>();

            points.Add(new Point(this.Area.X, this.Area.Y));
            points.Add(new Point(this.Area.X + this.PushX, this.Area.Y - this.PushY));
            points.Add(new Point(this.Area.X + this.PushX, this.Area.Y - this.PushY - (this.Height * this.SideLenght)));
            points.Add(new Point(this.Area.X, this.Area.Y - (2 * this.PushY) - (this.Height * this.SideLenght)));
            points.Add(new Point(this.Area.X - this.PushX, this.Area.Y - this.PushY - (this.Height * this.SideLenght)));
            points.Add(new Point(this.Area.X - this.PushX, this.Area.Y - this.PushY));

            StreamGeometry streamGeometry = new StreamGeometry();
            using (StreamGeometryContext streamGeometryContext = streamGeometry.Open())
            {
                streamGeometryContext.BeginFigure(points[0], true, true);
                streamGeometryContext.PolyLineTo(points, true, true);
            }

            return streamGeometry;
        }
    }
}
