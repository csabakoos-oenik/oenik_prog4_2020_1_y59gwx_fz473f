﻿// <copyright file="Water.cs" company="Obuda University John von Neumann Faculty of Informatics">
// Copyright (c) Obuda University John von Neumann Faculty of Informatics. All rights reserved.
// </copyright>

namespace CUBICTUNES.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Thos represents a water tile on the map.
    /// </summary>
    public class Water : Floor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Water"/> class.
        /// </summary>
        /// <param name="x">X coordinate.</param>
        /// <param name="y">Y coordinate.</param>
        /// <param name="lenght">Floor tile's side's lenght.</param>
        public Water(double x, double y, double lenght)
            : base(x, y, lenght)
        {
        }
    }
}
