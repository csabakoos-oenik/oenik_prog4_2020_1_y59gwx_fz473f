﻿// <copyright file="MapObject.cs" company="Obuda University John von Neumann Faculty of Informatics">
// Copyright (c) Obuda University John von Neumann Faculty of Informatics. All rights reserved.
// </copyright>

namespace CUBICTUNES.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// The parent class for every object on the map.
    /// </summary>
    public class MapObject
    {
        /// <summary>
        /// Private area property.
        /// </summary>
        private Rect area;

        /// <summary>
        /// Initializes a new instance of the <see cref="MapObject"/> class.
        /// The MapObject's constructor.
        /// </summary>
        /// <param name="x">X coordinate.</param>
        /// <param name="y">Y coordinate.</param>
        /// <param name="width">Object's width.</param>
        /// <param name="height">Object's height.</param>
        public MapObject(double x, double y, double width, double height)
        {
            this.area = new Rect(x, y, width, height);
        }

        /// <summary>
        /// Gets or sets the Object's area is represented by this Rect.
        /// </summary>
        public Rect Area
        {
            get { return this.area; }
            set { this.area = value; }
        }

        /// <summary>
        /// Changes the X coordinate of the object.
        /// </summary>
        /// <param name="difference">The amount the new Y value will differ from the old.</param>
        public void ChangeX(int difference)
        {
            this.area.X += difference;
        }

        /// <summary>
        /// Changes the Y coordinate of the object.
        /// </summary>
        /// <param name="difference">The amount the new Y value will differ from the old.</param>
        public void ChangeY(int difference)
        {
            this.area.Y += difference;
        }
    }
}
