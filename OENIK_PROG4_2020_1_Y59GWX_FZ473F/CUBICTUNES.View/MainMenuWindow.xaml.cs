﻿// <copyright file="MainMenuWindow.xaml.cs" company="Obuda University John von Neumann Faculty of Informatics">
// Copyright (c) Obuda University John von Neumann Faculty of Informatics. All rights reserved.
// </copyright>

namespace CUBICTUNES.View
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for MainMenuWindow.xaml.
    /// </summary>
    public partial class MainMenuWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenuWindow"/> class.
        /// </summary>
        public MainMenuWindow()
        {
            this.InitializeComponent();
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            LevelWindow levelWindow = new LevelWindow();
            this.Close();
            levelWindow.ShowDialog();
        }

        private void HighscoresButton_Click(object sender, RoutedEventArgs e)
        {
            HighScoresWindow highScoresWindow = new HighScoresWindow();
            this.DialogResult = true;
            highScoresWindow.ShowDialog();
        }

        private void ChangeProfileButton_Click(object sender, RoutedEventArgs e)
        {
            SelectProfileWindow selectProfile = new SelectProfileWindow();
            this.Close();
            selectProfile.ShowDialog();
        }
    }
}
