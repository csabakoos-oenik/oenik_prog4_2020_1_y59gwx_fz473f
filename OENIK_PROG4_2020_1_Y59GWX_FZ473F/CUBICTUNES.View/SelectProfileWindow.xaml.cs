﻿// <copyright file="SelectProfileWindow.xaml.cs" company="Obuda University John von Neumann Faculty of Informatics">
// Copyright (c) Obuda University John von Neumann Faculty of Informatics. All rights reserved.
// </copyright>

namespace CUBICTUNES.View
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using CUBICTUNES.ViewModel;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Interaction logic for SelectProfileWindow.xaml.
    /// </summary>
    public partial class SelectProfileWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SelectProfileWindow"/> class.
        /// </summary>
        public SelectProfileWindow()
        {
            this.InitializeComponent();
        }

        private void SelectProfileButton_Click(object sender, RoutedEventArgs e)
        {
            MainMenuWindow mainMenuWindow = new MainMenuWindow();
            this.Close();
            mainMenuWindow.ShowDialog();
        }

        private void DeleteProfileButton_Click(object sender, RoutedEventArgs e)
        {
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow(true);
            this.Close();
            main.ShowDialog();
        }

        private void DeleteProfile(string x)
        {
            (this.DataContext as SelectProfileViewModel).AllProfiles.Remove((this.DataContext as SelectProfileViewModel).SelectedProfile);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Register<string>(this, x => this.DeleteProfile(x));
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Messenger.Default.Unregister(this);
        }
    }
}
