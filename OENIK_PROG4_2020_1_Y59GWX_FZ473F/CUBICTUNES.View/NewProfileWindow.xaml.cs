﻿// <copyright file="NewProfileWindow.xaml.cs" company="Obuda University John von Neumann Faculty of Informatics">
// Copyright (c) Obuda University John von Neumann Faculty of Informatics. All rights reserved.
// </copyright>

namespace CUBICTUNES.View
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Interaction logic for NewProfileWindow.xaml.
    /// </summary>
    public partial class NewProfileWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NewProfileWindow"/> class.
        /// </summary>
        public NewProfileWindow()
        {
            this.InitializeComponent();
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow(true);
            this.DialogResult = true;
            main.ShowDialog();
        }

        private void CreateProfileButton_Click(object sender, RoutedEventArgs e)
        {
        }

        private void CreateProfile(string x)
        {
            MainMenuWindow mainMenuWindow = new MainMenuWindow();
            this.DialogResult = true;
            mainMenuWindow.ShowDialog();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Register<string>(this, x => this.CreateProfile(x));
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Messenger.Default.Unregister(this);
        }
    }
}
