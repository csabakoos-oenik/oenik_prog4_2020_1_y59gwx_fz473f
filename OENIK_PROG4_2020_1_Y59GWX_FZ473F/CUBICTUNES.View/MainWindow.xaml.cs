﻿// <copyright file="MainWindow.xaml.cs" company="Obuda University John von Neumann Faculty of Informatics">
// Copyright (c) Obuda University John von Neumann Faculty of Informatics. All rights reserved.
// </copyright>

namespace CUBICTUNES.View
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Media;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for MainWindow.xaml.
    /// </summary>
    public partial class MainWindow : Window
    {
        private MediaPlayer mainThemePlayer = new MediaPlayer();

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();

            this.mainThemePlayer.Open(new Uri("../../sounds/bensound-love.mp3", UriKind.Relative));
            this.mainThemePlayer.MediaEnded += new EventHandler(this.MediaEnded);
            this.mainThemePlayer.Play();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        /// <param name="isMusicOn">Determines if the music is already playing.</param>
        public MainWindow(bool isMusicOn)
            : this()
        {
            this.mainThemePlayer.Stop();
        }

        private void MediaEnded(object sender, EventArgs e)
        {
            this.mainThemePlayer.Position = TimeSpan.Zero;
            this.mainThemePlayer.Play();
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void NewProfileButton_Click(object sender, RoutedEventArgs e)
        {
            NewProfileWindow newProfile = new NewProfileWindow();
            this.Close();
            newProfile.ShowDialog();
        }

        private void SelectProfileButton_Click(object sender, RoutedEventArgs e)
        {
            SelectProfileWindow selectProfile = new SelectProfileWindow();
            this.Close();
            selectProfile.ShowDialog();
        }
    }
}
