﻿// <copyright file="GameControl.cs" company="Obuda University John von Neumann Faculty of Informatics">
// Copyright (c) Obuda University John von Neumann Faculty of Informatics. All rights reserved.
// </copyright>

namespace CUBICTUNES.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using CUBICTUNES.Logic;
    using CUBICTUNES.Model;

    /// <summary>
    /// The game's control.
    /// </summary>
    public class GameControl : FrameworkElement
    {
        private GameLogic gameLogic;
        private GameModel gameModel;
        private GameRenderer gameRenderer;


        /// <summary>
        /// Initializes a new instance of the <see cref="GameControl"/> class.
        /// </summary>
        public GameControl()
        {
            this.Loaded += this.GameControl_Loaded;
        }

        /// <summary>
        /// Gets or sets the current profile.
        /// </summary>
        public static Logic.Profile CurrentProfile { get; set; }

        /// <inheritdoc/>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.gameRenderer != null)
            {
                this.gameRenderer.Rendering(drawingContext);
            }
        }

        private void GameControl_Loaded(object sender, RoutedEventArgs e)
        {
            Window win = Window.GetWindow(this);

            this.gameModel = new GameModel(1200, 800, 50);
            this.gameLogic = new GameLogic(this.gameModel, CurrentProfile);
            this.gameLogic.InitLevelResource();
            this.gameLogic.InitLevel();
            this.gameRenderer = new GameRenderer(this.gameModel);

            this.gameLogic.RefreshScreen += (obj, args) => this.InvalidateVisual();
            this.InvalidateVisual();
        }
    }
}
