﻿// <copyright file="SelectProfileViewModel.cs" company="Obuda University John von Neumann Faculty of Informatics">
// Copyright (c) Obuda University John von Neumann Faculty of Informatics. All rights reserved.
// </copyright>

namespace CUBICTUNES.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CUBICTUNES.Logic;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.CommandWpf;

    /// <summary>
    /// Select profiles view model.
    /// </summary>
    public class SelectProfileViewModel : ViewModelBase
    {
        /// <summary>
        /// Currently selected profile from the list.
        /// </summary>
        private Profile selectedProfile;

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectProfileViewModel"/> class.
        /// </summary>
        public SelectProfileViewModel()
        {
            this.AllProfiles = new ObservableCollection<Profile>();

            if (this.IsInDesignMode)
            {
                this.AllProfiles.Add(new Profile("Mertzy", 1, 0));
                this.AllProfiles.Add(new Profile("SunlessKhan", 5, 12));
                this.AllProfiles.Add(new Profile("amustycow", 3, 7));
            }
            else
            {
                var tmpProfiles = Logic.Profiles.Default.GetAllProfiles();

                foreach (Logic.Profile profile in tmpProfiles)
                {
                    Profile vmProfile = new Profile(profile.Name, profile.Stage, profile.Stars);
                    this.AllProfiles.Add(vmProfile);
                }
            }

            this.Highscores = this.AllProfiles.OrderByDescending(x => x.Stars).ToList();

            this.DeleteProfile = new RelayCommand(() => Profiles.Default.DeleteProfile(this.selectedProfile.Name));
        }

        /// <summary>
        /// Gets the current profiles.
        /// </summary>
        public ObservableCollection<Profile> AllProfiles { get; private set; }

        /// <summary>
        /// Gets the highscores.
        /// </summary>
        public List<Profile> Highscores { get; private set; }

        /// <summary>
        /// Gets or sets the selected profile.
        /// </summary>
        public Profile SelectedProfile
        {
            get
            {
                return this.selectedProfile;
            }

            set
            {
                this.Set(ref this.selectedProfile, value);
                try
                {
                    GameControl.CurrentProfile = new Logic.Profile(this.selectedProfile.Name, this.selectedProfile.Stage, this.selectedProfile.Stars);
                }
                catch (NullReferenceException)
                {
                    GameControl.CurrentProfile = new Logic.Profile(string.Empty, 1, 0);
                }
            }
        }

        /// <summary>
        /// Gets delete profile command.
        /// </summary>
        public ICommand DeleteProfile { get; private set; }
    }
}
