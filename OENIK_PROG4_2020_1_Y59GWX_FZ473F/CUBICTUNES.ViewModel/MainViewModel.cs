﻿// <copyright file="MainViewModel.cs" company="Obuda University John von Neumann Faculty of Informatics">
// Copyright (c) Obuda University John von Neumann Faculty of Informatics. All rights reserved.
// </copyright>

namespace CUBICTUNES.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// This class is the main view model.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
        {
            this.Name = GameControl.CurrentProfile.Name;
            this.Level = GameControl.CurrentProfile.Stage;
            this.Stars = GameControl.CurrentProfile.Stars;
        }

        /// <summary>
        /// Gets or sets the profile's name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the current level.
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// Gets or sets the number of stars.
        /// </summary>
        public int Stars { get; set; }
    }
}
