﻿// <copyright file="ProfileViewModel.cs" company="Obuda University John von Neumann Faculty of Informatics">
// Copyright (c) Obuda University John von Neumann Faculty of Informatics. All rights reserved.
// </copyright>

namespace CUBICTUNES.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CUBICTUNES.Logic;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.CommandWpf;

    /// <summary>
    /// This is the profile's view model.
    /// </summary>
    public class ProfileViewModel : ViewModelBase
    {
        /// <summary>
        /// New profile's name.
        /// </summary>
        private string newProfileName;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProfileViewModel"/> class.
        /// </summary>
        public ProfileViewModel()
        {
            this.AddNewProfileCommand = new RelayCommand(() => Profiles.Default.AddNewProfile(this.newProfileName));
        }

        /// <summary>
        /// Gets or sets the newly created profile's name.
        /// </summary>
        public string NewProfileName
        {
            get
            {
                return this.newProfileName;
            }

            set
            {
                this.newProfileName = value;
                GameControl.CurrentProfile = new Logic.Profile(this.newProfileName, 1, 0);
            }
        }

        /// <summary>
        /// Gets an add new player command.
        /// </summary>
        public ICommand AddNewProfileCommand { get; private set; }
    }
}