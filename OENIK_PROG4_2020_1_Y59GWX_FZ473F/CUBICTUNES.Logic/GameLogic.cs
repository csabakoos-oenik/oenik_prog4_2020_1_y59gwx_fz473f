﻿// <copyright file="GameLogic.cs" company="Obuda University John von Neumann Faculty of Informatics">
// Copyright (c) Obuda University John von Neumann Faculty of Informatics. All rights reserved.
// </copyright>

namespace CUBICTUNES.Logic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using CUBICTUNES.Model;

    /// <summary>
    /// The game's logic.
    /// </summary>
    public class GameLogic : IGameLogic
    {
        /// <summary>
        /// This field stores the current level.
        /// </summary>
        private int currentLevel;

        /// <summary>
        /// The used game model class.
        /// </summary>
        private GameModel gameModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="gameModel">Dependency injection. The used game model.</param>
        /// <param name="currentProfile">The current profile.</param>
        public GameLogic(GameModel gameModel, Profile currentProfile)
        {
            this.gameModel = gameModel;
            this.CurrentProfile = currentProfile;
            this.currentLevel = 1;
        }

        /// <inheritdoc/>
        public event EventHandler RefreshScreen;

        /// <summary>
        /// Gets or sets the current profile.
        /// </summary>
        public Profile CurrentProfile { get; set; }

        /// <inheritdoc/>
        public void CountDown()
        {
            this.gameModel.PlayerTime.Start();
        }

        /// <inheritdoc/>
        public string InitLevelResource()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = $"CUBICTUNES.Logic.levels.{this.currentLevel.ToString("00")}.lvl";

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

        /// <inheritdoc/>
        public void InitLevel()
        {
            string[] mapHelper = this.InitLevelResource().Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);

            this.gameModel.Map = new MapObject[int.Parse(mapHelper[0].Split('x')[0]), int.Parse(mapHelper[0].Split('x')[1])];
            this.gameModel.MapBase = new MapObject[int.Parse(mapHelper[0].Split('x')[0]), int.Parse(mapHelper[0].Split('x')[1])];

            for (int i = 1; i < mapHelper.Length; i++)
            {
                string[] rowHelper = mapHelper[i].Split('|');

                i--;

                for (int j = 0; j < rowHelper.Length; j++)
                {
                    string[] fieldHepler = rowHelper[j].Split('.');

                    switch (fieldHepler[0])
                    {
                        case "-1":
                            this.gameModel.MapBase[i, j] = new Water(
                            (this.gameModel.GameWidth / 2) + (j * this.gameModel.PushX) - (i * this.gameModel.PushX),
                            (this.gameModel.GameHeight / 2) - ((this.gameModel.Map.GetLength(1) * this.gameModel.BlockSize) / 3) + ((((2 * i) + j) * this.gameModel.PushY) - (i * this.gameModel.PushY)),
                            this.gameModel.BlockSize);
                            break;
                        default:
                            this.gameModel.MapBase[i, j] = new Floor(
                            (this.gameModel.GameWidth / 2) + (j * this.gameModel.PushX) - (i * this.gameModel.PushX),
                            (this.gameModel.GameHeight / 2) - ((this.gameModel.Map.GetLength(1) * this.gameModel.BlockSize) / 3) + ((((2 * i) + j) * this.gameModel.PushY) - (i * this.gameModel.PushY)),
                            this.gameModel.BlockSize);

                            if (fieldHepler.Length > 1)
                            {
                                if (fieldHepler[1] == "E")
                                {
                                    this.gameModel.Map[i, j] = new Cube(
                                    (this.gameModel.GameWidth / 2) + (j * this.gameModel.PushX) - (i * this.gameModel.PushX),
                                    (this.gameModel.GameHeight / 2) - ((this.gameModel.Map.GetLength(1) * this.gameModel.BlockSize) / 3) + ((((2 * i) + j) * this.gameModel.PushY) - (i * this.gameModel.PushY)),
                                    this.gameModel.BlockSize,
                                    1,
                                    CubeTypes.Player);
                                }

                                if (fieldHepler[1] == "Q")
                                {
                                    this.gameModel.Map[i, j] = new Cube(
                                    (this.gameModel.GameWidth / 2) + (j * this.gameModel.PushX) - (i * this.gameModel.PushX),
                                    (this.gameModel.GameHeight / 2) - ((this.gameModel.Map.GetLength(1) * this.gameModel.BlockSize) / 3) + ((((2 * i) + j) * this.gameModel.PushY) - (i * this.gameModel.PushY)),
                                    this.gameModel.BlockSize,
                                    1,
                                    CubeTypes.Exit);
                                }
                            }

                            break;
                    }

                    switch (fieldHepler[0])
                    {
                        case "S":
                            this.gameModel.MapBase[i, j] = new Cube(
                            (this.gameModel.GameWidth / 2) + (j * this.gameModel.PushX) - (i * this.gameModel.PushX),
                            (this.gameModel.GameHeight / 2) - ((this.gameModel.Map.GetLength(1) * this.gameModel.BlockSize) / 3) + ((((2 * i) + j) * this.gameModel.PushY) - (i * this.gameModel.PushY)),
                            this.gameModel.BlockSize,
                            int.Parse(fieldHepler[1]),
                            CubeTypes.Stationary);

                            if (fieldHepler.Length > 2)
                            {
                                if (fieldHepler[2] == "E")
                                {
                                    this.gameModel.Map[i, j] = new Cube(
                                    (this.gameModel.GameWidth / 2) + (j * this.gameModel.PushX) - (i * this.gameModel.PushX),
                                    (this.gameModel.GameHeight / 2) - ((this.gameModel.Map.GetLength(1) * this.gameModel.BlockSize) / 3) + ((((2 * i) + j) * this.gameModel.PushY) - (i * this.gameModel.PushY)),
                                    this.gameModel.BlockSize,
                                    int.Parse(fieldHepler[1]) + 1,
                                    CubeTypes.Player);
                                }

                                if (fieldHepler[2] == "Q")
                                {
                                    this.gameModel.Map[i, j] = new Cube(
                                    (this.gameModel.GameWidth / 2) + (j * this.gameModel.PushX) - (i * this.gameModel.PushX),
                                    (this.gameModel.GameHeight / 2) - ((this.gameModel.Map.GetLength(1) * this.gameModel.BlockSize) / 3) + ((((2 * i) + j) * this.gameModel.PushY) - (i * this.gameModel.PushY)),
                                    this.gameModel.BlockSize,
                                    int.Parse(fieldHepler[1]) + 1,
                                    CubeTypes.Exit);
                                }
                            }

                            break;
                        case "G":
                            this.gameModel.Map[i, j] = new Cube(
                            (this.gameModel.GameWidth / 2) + (j * this.gameModel.PushX) - (i * this.gameModel.PushX),
                            (this.gameModel.GameHeight / 2) - ((this.gameModel.Map.GetLength(1) * this.gameModel.BlockSize) / 3) + ((((2 * i) + j) * this.gameModel.PushY) - (i * this.gameModel.PushY)),
                            this.gameModel.BlockSize,
                            int.Parse(fieldHepler[1]),
                            CubeTypes.Green);

                            if (fieldHepler.Length > 2)
                            {
                                if (fieldHepler[2] == "E")
                                {
                                    this.gameModel.Map[i, j] = new Cube(
                                    (this.gameModel.GameWidth / 2) + (j * this.gameModel.PushX) - (i * this.gameModel.PushX),
                                    (this.gameModel.GameHeight / 2) - ((this.gameModel.Map.GetLength(1) * this.gameModel.BlockSize) / 3) + ((((2 * i) + j) * this.gameModel.PushY) - (i * this.gameModel.PushY)),
                                    this.gameModel.BlockSize,
                                    int.Parse(fieldHepler[1]) + 1,
                                    CubeTypes.Player);
                                }

                                if (fieldHepler[2] == "Q")
                                {
                                    this.gameModel.Map[i, j] = new Cube(
                                    (this.gameModel.GameWidth / 2) + (j * this.gameModel.PushX) - (i * this.gameModel.PushX),
                                    (this.gameModel.GameHeight / 2) - ((this.gameModel.Map.GetLength(1) * this.gameModel.BlockSize) / 3) + ((((2 * i) + j) * this.gameModel.PushY) - (i * this.gameModel.PushY)),
                                    this.gameModel.BlockSize,
                                    int.Parse(fieldHepler[1]) + 1,
                                    CubeTypes.Exit);
                                }
                            }

                            break;
                        case "P":
                            this.gameModel.Map[i, j] = new Cube(
                            (this.gameModel.GameWidth / 2) + (j * this.gameModel.PushX) - (i * this.gameModel.PushX),
                            (this.gameModel.GameHeight / 2) - ((this.gameModel.Map.GetLength(1) * this.gameModel.BlockSize) / 3) + ((((2 * i) + j) * this.gameModel.PushY) - (i * this.gameModel.PushY)),
                            this.gameModel.BlockSize,
                            int.Parse(fieldHepler[1]),
                            CubeTypes.Purple);

                            if (fieldHepler.Length > 2)
                            {
                                if (fieldHepler[2] == "E")
                                {
                                    this.gameModel.Map[i, j] = new Cube(
                                    (this.gameModel.GameWidth / 2) + (j * this.gameModel.PushX) - (i * this.gameModel.PushX),
                                    (this.gameModel.GameHeight / 2) - ((this.gameModel.Map.GetLength(1) * this.gameModel.BlockSize) / 3) + ((((2 * i) + j) * this.gameModel.PushY) - (i * this.gameModel.PushY)),
                                    this.gameModel.BlockSize,
                                    int.Parse(fieldHepler[1]) + 1,
                                    CubeTypes.Player);
                                }

                                if (fieldHepler[2] == "Q")
                                {
                                    this.gameModel.Map[i, j] = new Cube(
                                    (this.gameModel.GameWidth / 2) + (j * this.gameModel.PushX) - (i * this.gameModel.PushX),
                                    (this.gameModel.GameHeight / 2) - ((this.gameModel.Map.GetLength(1) * this.gameModel.BlockSize) / 3) + ((((2 * i) + j) * this.gameModel.PushY) - (i * this.gameModel.PushY)),
                                    this.gameModel.BlockSize,
                                    int.Parse(fieldHepler[1]) + 1,
                                    CubeTypes.Exit);
                                }
                            }

                            break;
                    }
                }

                i++;
            }
        }

        /// <inheritdoc/>
        public void JumpPlayer()
        {
            this.RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        /// <inheritdoc/>
        public void MoveCube()
        {
            this.RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        /// <inheritdoc/>
        public void MovePlayer()
        {
            this.RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        /// <inheritdoc/>
        public void Win()
        {
            this.gameModel.PlayerTime.Stop();
        }
    }
}
