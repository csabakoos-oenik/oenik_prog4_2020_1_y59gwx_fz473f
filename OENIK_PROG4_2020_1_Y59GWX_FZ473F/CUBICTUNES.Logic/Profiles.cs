﻿// <copyright file="Profiles.cs" company="Obuda University John von Neumann Faculty of Informatics">
// Copyright (c) Obuda University John von Neumann Faculty of Informatics. All rights reserved.
// </copyright>

namespace CUBICTUNES.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CUBICTUNES.Repository;

    /// <summary>
    /// The singleton class that has every record of the Profiles table.
    /// </summary>
    public class Profiles : IProfilesLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Profiles"/> class.
        /// </summary>
        public Profiles()
        {
        }

        /// <summary>
        /// Gets the single instance of the Profiles.
        /// </summary>
        public static Profiles Default
        {
            get { return new Profiles(); }
        }

        /// <summary>
        /// Gets profiles's list.
        /// </summary>
        /// <returns>The profile's list.</returns>
        public List<Profile> GetAllProfiles()
        {
            List<Profile> logicProfiles = new List<Profile>();
            List<Repository.Profile> repositoryProfiles = Repository.Profiles.Default.GetAllProfiles();

            foreach (Repository.Profile profile in repositoryProfiles)
            {
                Profile newProfile = new Profile(profile.Name, profile.Stage, profile.Stars);
                logicProfiles.Add(newProfile);
            }

            return logicProfiles;
        }

        /// <summary>
        /// This method will add a new profile to the Repository Profiles singelton class.
        /// </summary>
        /// <param name="name">New profile's name.</param>
        public void AddNewProfile(string name)
        {
            Repository.Profiles.Default.AddNewProfile(name);
        }

        /// <summary>
        /// This method will delete a selected profile from the Repository Profiles singelton class.
        /// </summary>
        /// <param name="name">To be deleted profile's name.</param>
        public void DeleteProfile(string name)
        {
            Repository.Profiles.Default.DeleteProfile(name);
        }
    }
}
