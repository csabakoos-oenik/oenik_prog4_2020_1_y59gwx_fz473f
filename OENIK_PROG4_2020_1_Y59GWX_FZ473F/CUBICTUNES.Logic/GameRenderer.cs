﻿// <copyright file="GameRenderer.cs" company="Obuda University John von Neumann Faculty of Informatics">
// Copyright (c) Obuda University John von Neumann Faculty of Informatics. All rights reserved.
// </copyright>

namespace CUBICTUNES.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using CUBICTUNES.Model;

    /// <summary>
    /// The game's renderer.
    /// </summary>
    public class GameRenderer
    {
        private GameModel gameModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameRenderer"/> class.
        /// </summary>
        /// <param name="gameModel">Game model.</param>
        public GameRenderer(GameModel gameModel)
        {
            this.gameModel = gameModel;
        }

        /// <summary>
        /// This method renders the game objects.
        /// </summary>
        /// <param name="context">Drawing context.</param>
        public void Rendering(DrawingContext context)
        {
            DrawingGroup drawingGroup = new DrawingGroup();

            for (int i = 0; i < this.gameModel.MapBase.GetLength(0); i++)
            {
                for (int j = 0; j < this.gameModel.MapBase.GetLength(1); j++)
                {
                    if (this.gameModel.MapBase[i, j] is Floor)
                    {
                        drawingGroup.Children.Add(new GeometryDrawing(Brushes.Transparent, new Pen(Brushes.White, 3), (this.gameModel.MapBase[i, j] as Floor).GetGeometry()));
                    }

                    if (this.gameModel.MapBase[i, j] is Water)
                    {
                        drawingGroup.Children.Add(new GeometryDrawing(Brushes.Aqua, new Pen(Brushes.Aquamarine, 3), (this.gameModel.MapBase[i, j] as Water).GetGeometry()));
                    }

                    if (this.gameModel.MapBase[i, j] is Cube)
                    {
                        drawingGroup.Children.Add(new GeometryDrawing(Brushes.Gray, null, (this.gameModel.MapBase[i, j] as Cube).GetGeometry()));
                    }

                    if (this.gameModel.Map[i, j] is Cube)
                    {
                        switch ((this.gameModel.Map[i, j] as Cube).CubeType)
                        {
                            case CubeTypes.Purple:
                                drawingGroup.Children.Add(new GeometryDrawing(Brushes.Purple, null, (this.gameModel.Map[i, j] as Cube).GetGeometry()));
                                break;
                            case CubeTypes.Green:
                                drawingGroup.Children.Add(new GeometryDrawing(Brushes.Green, null, (this.gameModel.Map[i, j] as Cube).GetGeometry()));
                                break;
                            case CubeTypes.Red:
                                drawingGroup.Children.Add(new GeometryDrawing(Brushes.Red, null, (this.gameModel.Map[i, j] as Cube).GetGeometry()));
                                break;
                            case CubeTypes.Blue:
                                drawingGroup.Children.Add(new GeometryDrawing(Brushes.Blue, null, (this.gameModel.Map[i, j] as Cube).GetGeometry()));
                                break;
                            case CubeTypes.Player:
                                drawingGroup.Children.Add(new GeometryDrawing(Brushes.Transparent, new Pen(Brushes.Gold, 3), (this.gameModel.Map[i, j] as Cube).GetGeometry()));
                                break;
                            case CubeTypes.Exit:
                                drawingGroup.Children.Add(new GeometryDrawing(Brushes.Transparent, new Pen(Brushes.Salmon, 3), (this.gameModel.Map[i, j] as Cube).GetGeometry()));
                                break;
                        }
                    }
                }
            }

            context.DrawDrawing(drawingGroup);
        }
    }
}
