﻿// <copyright file="IGameLogic.cs" company="Obuda University John von Neumann Faculty of Informatics">
// Copyright (c) Obuda University John von Neumann Faculty of Informatics. All rights reserved.
// </copyright>

namespace CUBICTUNES.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CUBICTUNES.Model;

    /// <summary>
    /// This interface declares what game logic methods will have to be written.
    /// </summary>
    public interface IGameLogic
    {
        /// <summary>
        /// Event handler that is called when change is occured in the map.
        /// </summary>
        event EventHandler RefreshScreen;

        /// <summary>
        /// This method describes what will happen when the player completes the current level.
        /// </summary>
        void Win();

        /// <summary>
        /// This method moves the player on the map.
        /// </summary>
        void MovePlayer();

        /// <summary>
        /// This method jumps the player onto a cube.
        /// </summary>
        void JumpPlayer();

        /// <summary>
        /// This method moves a cube.
        /// </summary>
        void MoveCube();

        /// <summary>
        /// This method starts a countdown in order to determine how many stars the player will recieve at the end of the map.
        /// </summary>
        void CountDown();

        /// <summary>
        /// This method initialises the levels for the game.
        /// </summary>
        /// <returns>The embedded level as string.</returns>
        string InitLevelResource();

        /// <summary>
        /// This method initialises the model's map from the embedded resources.
        /// </summary>
        void InitLevel();
    }
}
