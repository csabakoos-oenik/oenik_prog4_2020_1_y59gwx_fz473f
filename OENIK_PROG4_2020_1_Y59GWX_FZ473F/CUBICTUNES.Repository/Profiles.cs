﻿// <copyright file="Profiles.cs" company="Obuda University John von Neumann Faculty of Informatics">
// Copyright (c) Obuda University John von Neumann Faculty of Informatics. All rights reserved.
// </copyright>

namespace CUBICTUNES.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CUBICTUNES.Data;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// The singleton class that has every record of the Profiles table.
    /// </summary>
    public class Profiles : IProfilesRepository
    {
        /// <summary>
        /// The private database entity instance.
        /// </summary>
        private CubicTunesDatabaseEntities db = new CubicTunesDatabaseEntities();

        /// <summary>
        /// Initializes a new instance of the <see cref="Profiles"/> class.
        /// </summary>
        public Profiles()
        {
        }

        /// <summary>
        /// Gets public instance.
        /// </summary>
        public static Profiles Default
        {
            get { return new Profiles(); }
        }

        /// <summary>
        /// Gets profiles's list.
        /// </summary>
        /// <returns>Profile's list.</returns>
        public List<Profile> GetAllProfiles()
        {
            List<Data.Profile> dbProfiles = this.db.Profiles.ToList();
            List<Profile> profiles = new List<Profile>();

            foreach (Data.Profile dbprofile in dbProfiles)
                {
                    profiles.Add(new Profile(dbprofile.Name, dbprofile.Stage, dbprofile.Stars));
                }

            return profiles;
        }

        /// <summary>
        /// This method will add a new profile into the Profiles table.
        /// </summary>
        /// <param name="name">The new profile's name.</param>
        public void AddNewProfile(string name)
        {
            Data.Profile newProfile = new Data.Profile()
            {
                Name = name,
                Stage = 1,
                Stars = 0,
            };

            this.db.Profiles.Add(newProfile);
            this.db.SaveChanges();

            Messenger.Default.Send("Profile created!");
        }

        /// <summary>
        /// This method will delete a selected profile from the Profiles table.
        /// </summary>
        /// <param name="name">The profile to be deleted.</param>
        public void DeleteProfile(string name)
        {
            this.db.Profiles.Remove(this.db.Profiles.First(x => x.Name == name));
            this.db.SaveChanges();

            Messenger.Default.Send("Profile deleted!");
        }
    }
}
