﻿// <copyright file="IProfilesRepository.cs" company="Obuda University John von Neumann Faculty of Informatics">
// Copyright (c) Obuda University John von Neumann Faculty of Informatics. All rights reserved.
// </copyright>

namespace CUBICTUNES.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Repository profile's interface.
    /// </summary>
    public interface IProfilesRepository
    {
        /// <summary>
        /// Gets profiles's list.
        /// </summary>
        /// <returns>Profile's list.</returns>
        List<Profile> GetAllProfiles();

        /// <summary>
        /// This method will add a new profile into the Profiles table.
        /// </summary>
        /// <param name="name">The new profile's name.</param>
        void AddNewProfile(string name);

        /// <summary>
        /// This method will delete a selected profile from the Profiles table.
        /// </summary>
        /// <param name="name">The profile to be deleted.</param>
        void DeleteProfile(string name);
    }
}
