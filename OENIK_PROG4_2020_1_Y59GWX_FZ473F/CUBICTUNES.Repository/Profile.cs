﻿// <copyright file="Profile.cs" company="Obuda University John von Neumann Faculty of Informatics">
// Copyright (c) Obuda University John von Neumann Faculty of Informatics. All rights reserved.
// </copyright>

namespace CUBICTUNES.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// This class represents a single record in the Profiles table.
    /// </summary>
    public class Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Profile"/> class.
        /// </summary>
        /// <param name="name">The name which will be given to the user's profile.</param>
        /// <param name="stage">The latest unlocked level.</param>
        /// <param name="stars">The collected stars.</param>
        public Profile(string name, int stage, int stars)
        {
            this.Name = name;
            this.Stage = stage;
            this.Stars = stars;
        }

        /// <summary>
        /// Gets or sets the user's name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the latest level which the user has unlocked.
        /// </summary>
        public int Stage { get; set; }

        /// <summary>
        /// Gets or sets the number of collected stars.
        /// </summary>
        public int Stars { get; set; }
    }
}
