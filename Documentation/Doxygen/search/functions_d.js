var searchData=
[
  ['profile_324',['Profile',['../class_c_u_b_i_c_t_u_n_e_s_1_1_logic_1_1_profile.html#a25b3ced30cde869b05308596b51882c5',1,'CUBICTUNES.Logic.Profile.Profile()'],['../class_c_u_b_i_c_t_u_n_e_s_1_1_repository_1_1_profile.html#a19fb45e0742b714ccd97eb27e67248bb',1,'CUBICTUNES.Repository.Profile.Profile()'],['../class_c_u_b_i_c_t_u_n_e_s_1_1_view_model_1_1_profile.html#a5b6d803afc4c245665fe3c59291cecba',1,'CUBICTUNES.ViewModel.Profile.Profile()']]],
  ['profiles_325',['Profiles',['../class_c_u_b_i_c_t_u_n_e_s_1_1_logic_1_1_profiles.html#ae9be3bfff3b63ce6de628a52d9eb5648',1,'CUBICTUNES.Logic.Profiles.Profiles()'],['../class_c_u_b_i_c_t_u_n_e_s_1_1_repository_1_1_profiles.html#a26acf7eade80bfc14e7bb30900b9cc17',1,'CUBICTUNES.Repository.Profiles.Profiles()']]],
  ['profileviewmodel_326',['ProfileViewModel',['../class_c_u_b_i_c_t_u_n_e_s_1_1_view_model_1_1_profile_view_model.html#af75b50a4e6bd233b416f32e9db9f7e1f',1,'CUBICTUNES::ViewModel::ProfileViewModel']]]
];
