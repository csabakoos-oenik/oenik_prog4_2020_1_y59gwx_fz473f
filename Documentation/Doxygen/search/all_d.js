var searchData=
[
  ['name_126',['Name',['../class_c_u_b_i_c_t_u_n_e_s_1_1_data_1_1_profile.html#aaf8048c83a5cd4d2a3c729a918f245a8',1,'CUBICTUNES.Data.Profile.Name()'],['../class_c_u_b_i_c_t_u_n_e_s_1_1_logic_1_1_profile.html#a657657bc18ae615f6ea13edf337bfe8d',1,'CUBICTUNES.Logic.Profile.Name()'],['../class_c_u_b_i_c_t_u_n_e_s_1_1_repository_1_1_profile.html#ac55a6c3a0acea009c6cbce43eca85259',1,'CUBICTUNES.Repository.Profile.Name()'],['../class_c_u_b_i_c_t_u_n_e_s_1_1_view_model_1_1_main_view_model.html#add763b5542a2c2c5b44c5ecce61ef254',1,'CUBICTUNES.ViewModel.MainViewModel.Name()'],['../class_c_u_b_i_c_t_u_n_e_s_1_1_view_model_1_1_profile.html#adb3821134afa59d4d5e28842b94eb74d',1,'CUBICTUNES.ViewModel.Profile.Name()']]],
  ['newprofilename_127',['NewProfileName',['../class_c_u_b_i_c_t_u_n_e_s_1_1_view_model_1_1_profile_view_model.html#a3f1ff2620803f154ad9c8a341ec43150',1,'CUBICTUNES::ViewModel::ProfileViewModel']]],
  ['newprofilewindow_128',['NewProfileWindow',['../class_c_u_b_i_c_t_u_n_e_s_1_1_view_1_1_new_profile_window.html',1,'CUBICTUNES.View.NewProfileWindow'],['../class_c_u_b_i_c_t_u_n_e_s_1_1_view_1_1_new_profile_window.html#a97b6829fda03162e441776c2c97fd395',1,'CUBICTUNES.View.NewProfileWindow.NewProfileWindow()']]],
  ['newprofilewindow_2eg_2ecs_129',['NewProfileWindow.g.cs',['../_new_profile_window_8g_8cs.html',1,'']]],
  ['newprofilewindow_2eg_2ei_2ecs_130',['NewProfileWindow.g.i.cs',['../_new_profile_window_8g_8i_8cs.html',1,'']]],
  ['newprofilewindow_2examl_2ecs_131',['NewProfileWindow.xaml.cs',['../_new_profile_window_8xaml_8cs.html',1,'']]],
  ['notice_132',['notice',['../_t_h_i_r_d-_p_a_r_t_y-_n_o_t_i_c_e_s_8txt.html#afc72352a20a40545a10506a10de4607f',1,'notice():&#160;THIRD-PARTY-NOTICES.txt'],['../_t_h_i_r_d-_p_a_r_t_y-_n_o_t_i_c_e_s_8_t_x_t.html#afc72352a20a40545a10506a10de4607f',1,'notice():&#160;THIRD-PARTY-NOTICES.TXT']]]
];
